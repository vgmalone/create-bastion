# Create Instance

An Ansible script that allows me to quickly spin-up an EC2 instance for test/dev purposes. The AWS access key and Secret Access key are stored in Ansible Vault on my workstation. When launching the script, you will be asked for the Vault password before it executes.

- AMI: Amazon Linux 2023
- Type: t2.micro
